<?php

namespace Drupal\fontawesome_iconpicker_to_micon;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Core\Entity\EntityDisplayRepository;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;

/**
 * Service description.
 */
class FontawesomeIconpickerToMicoConversionHandler {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepository
   */
  protected $entityDisplayRepository;

  const SOURCE_FIELD_TYPE = 'string';
  const SOURCE_FIELD_FORMATTER_TYPE = 'fontawesome_iconpicker';
  const TARGET_FIELD_TYPE = 'string_micon';
  const TARGET_FIELD_FORMATTER_TYPE = 'string_micon';

  /**
   * Constructs a FontawesomeIconpickerToMicoConversionHandler object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepository $entity_display_repository
   *   The entity display repository.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, EntityDisplayRepository $entity_display_repository) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * Returns the convertible fields.
   *
   * Returns the fields by entity.field_name => label.
   *
   * @return array The fields to return.
   */
  public function getConvertibleFields() {
    $convertibleFields = [];
    $entityFormDisplays = $this->entityTypeManager->getStorage('entity_form_display')->loadMultiple();
    if (!empty($entityFormDisplays)) {
      foreach ($entityFormDisplays as $entityFormDisplay) {
        /** @var \Drupal\Core\Entity\Entity\EntityFormDisplay $entityFormDisplay */
        $components = $entityFormDisplay->getComponents();
        if (!empty($components)) {
          foreach ($components as $field_name => $component) {
            if (!empty($component['type']) && $component['type'] == self::SOURCE_FIELD_FORMATTER_TYPE) {
              $efdId = $entityFormDisplay->id();
              $identification = $efdId . '.' . $field_name;
              $convertibleFields[$identification] = $identification;
            }
          }
        }
      }
    }
    return $convertibleFields;
  }

  /**
   * Helper function to extract the field storage id.
   *
   * Expects a full form display component id like:
   * 'node.article.default.field_icon' and returns
   * 'node.article.default'
   *
   * @param mixed $entityFormDisplayComponentFullId
   *   The full form display component id.
   *
   * @return string
   *   The extracted entity form display id.
   */
  public static function extractEntityFormDisplayId($entityFormDisplayComponentFullId) {
    $parts = explode('.', $entityFormDisplayComponentFullId);
    if (count($parts) != 4) {
      throw new Exception('$entityFormDisplayComponentFullId must consist of 4 parts. Given value: ' . $entityFormDisplayComponentFullId);
    }
    return $parts[0] . '.' . $parts[1] . '.' . $parts[2];
  }

  /**
   * Helper function to extract the field storage id.
   *
   * Expects a full form display component id like:
   * 'node.article.default.field_icon' and returns
   * 'node.article.field_icon'
   *
   * @param mixed $entityFormDisplayComponentFullId
   *   The full form display component id.
   *
   * @return string
   *   The extracted field storage id.
   */
  public static function extractFieldStorageId($entityFormDisplayComponentFullId) {
    $parts = explode('.', $entityFormDisplayComponentFullId);
    if (count($parts) != 4) {
      throw new Exception('$entityFormDisplayComponentFullId must consist of 4 parts. Given value: ' . $entityFormDisplayComponentFullId);
    }
    return $parts[0] . '.' . $parts[3];
  }

  /**
   * Helper function to extract the field id.
   *
   * Expects a full form display component id like:
   * 'node.article.default.field_icon' and returns
   * 'field_icon'
   *
   * @param mixed $entityFormDisplayComponentFullId
   *   The full form display component id.
   *
   * @return string
   *   The extracted standalone field id.
   */
  public static function extractFieldId($entityFormDisplayComponentFullId) {
    $parts = explode('.', $entityFormDisplayComponentFullId);
    if (count($parts) != 4) {
      throw new Exception('$entityFormDisplayComponentFullId must consist of 4 parts. Given value: ' . $entityFormDisplayComponentFullId);
    }
    // Return only the field name:
    return $parts[3];
  }

  /**
   * Converts all fields.
   *
   * @return array
   *   The converted fields.
   */
  public function convertAll() {
    $convertibleFields = $this->getConvertibleFields();
    return $this->convertFields(array_keys($convertibleFields));
  }

  /**
   * Converts the given fields to micon fields.
   *
   * @param array $fieldsToConvert
   *   The fields to convert (entity.field)
   *
   * @return array
   *   The converted fields.
   */
  public function convertFields(array $fieldsToConvert) {
    $convertedFields = [];
    // Ensure the given fields are (still) convertible:
    $convertibleFields = $this->getConvertibleFields();
    $convertibleKeys = array_keys($convertibleFields);
    if (empty($convertibleKeys)) {
      throw new \Exception('No convertible fields.');
    }
    if (!empty($fieldsToConvert)) {
      foreach ($fieldsToConvert as $fieldToConvert) {
        if (!in_array($fieldToConvert, $convertibleKeys)) {
          throw new \Exception('Field ' . $fieldToConvert . ' is not convertible. Aborting.');
        }

        if ($this->convertField($fieldToConvert)) {
          // Return the successfully converted field:
          $convertedFields[] = $fieldToConvert;
        }
      }
    }
    return $convertedFields;
  }

  /**
   * Converts a single field from text_plain to string_micon.
   *
   * @param mixed $fieldToConvert
   * @return bool
   *   True if conversion was successful, else false.
   * @throws Exception
   */
  protected function convertField($entityFormDisplayComponentFullId) {
    $fieldStorageId = self::extractFieldStorageId($entityFormDisplayComponentFullId);
    $entityFormDisplayId = self::extractEntityFormDisplayId($entityFormDisplayComponentFullId);
    $fieldId = self::extractFieldId($entityFormDisplayComponentFullId);

    $fieldStorageConverted = $this->convertFieldStorage($fieldStorageId);
    $fieldFormDisplayConverted = $this->convertFieldFormDisplay($entityFormDisplayId, $fieldId);

    // Return true if both was successful:
    return $fieldStorageConverted && $fieldFormDisplayConverted;
  }

  /**
   * Helper function to convert the field storage from
   * SOURCE_FIELD_TYPE to TARGET_FIELD_TYPE.
   *
   * @param mixed $fieldStorageId
   *   The field storage ID.
   */
  protected function convertFieldStorage($fieldStorageId) {
    /**
     * @var \Drupal\field\Entity\FieldStorageConfig $fieldStorageConfig
     */
    $fieldStorageConfig = $this->entityTypeManager->getStorage('field_storage_config')->load($fieldStorageId);
    if (empty($fieldStorageConfig)) {
      throw new \Exception('Field storage for field "' . $fieldStorageId . '" could not be loaded');
    }
    // @todo: Proceed here!
    if (FALSE) {
      throw new \Exception('Given field "' . $fieldStorageId . '" is not of type text_plain and can not be convertet so.');
    }

    return $this->changeFieldStorageConfigType($fieldStorageConfig, self::TARGET_FIELD_TYPE);
  }

  /**
   * Helper function to change the type of a FieldStorageConfig.
   *
   * Due to Drupal Core's intended limitations of not being allowed
   * to change the type of already existing field storages,
   * this helper function based on the code on the documentation page at:
   * https://www.drupal.org/docs/drupal-apis/update-api/updating-entities-and-fields-in-drupal-8
   * Implements the bunch of code needed to do such a conversion.
   *
   * WARNING: This method is not applicable to all cases of changing
   * field types, just to cases where the schema of the old and new field type
   * are equal and just the field type name is different!
   * This ist the case for the parent module
   * fontawesome_iconpicker_to_micon which this
   * service is sticked to. But might not be for other cases!
   *
   * Furthermore, we do NOT update entity_view_display and entity_form_display
   * here, which might be required for most other cases, as we don't need it
   * in this module!
   * So better see
   * https://www.drupal.org/docs/drupal-apis/update-api/updating-entities-and-fields-in-drupal-8
   * for a more general solution!
   *
   * @param \Drupal\field\Entity\FieldStorageConfig $fieldStorageConfig
   *   The field storage config to change to $newType.
   * @param string $newType
   *   The new field type to use.
   *
   * @return bool
   *   Returns true if the field was converted, else false.
   */
  protected function changeFieldStorageConfigType(FieldStorageConfig $fieldStorageConfig, string $newType) {
    // Since the usual workflow for field storages do not allow changing the
    // field type, we have to work around it in this case.
    $new_field_storage = $fieldStorageConfig->toArray();
    $new_field_storage['type'] = $newType;

    // Create the new field storage to replace the old one:
    $new_field_storage = FieldStorageConfig::create($new_field_storage);
    $new_field_storage->original = $new_field_storage;
    $new_field_storage->enforceIsNew(FALSE);

    $new_field_storage->save();

    $field_name = $fieldStorageConfig->getName();
    $entity_type = $fieldStorageConfig->getTargetEntityTypeId();
    if (!$fields = $this->entityTypeManager->getStorage('field_config')->loadByProperties([
      'field_name' => $field_name,
      'entity_type' => $entity_type,
    ])) {
      // Has no field instances. So can't have any other uses.
      return FALSE;
    }

    foreach ($fields as $field) {
      /**
       * @var \Drupal\field\Entity\FieldConfig $field
       */
      $new_field = $field->toArray();
      $new_field['field_type'] = $newType;

      $new_field = FieldConfig::create($new_field);
      $new_field->original = $field;
      $new_field->enforceIsNew(FALSE);
      $new_field->save();
    }

    return TRUE;
  }

  /**
   * Helper function to convert the entity form field display formatter
   * from SOURCE_FIELD_FORMATTER_TYPE to TARGET_FIELD_FORMATTER_TYPE.
   *
   */
  protected function convertFieldFormDisplay($entityFormDisplayId, $fieldId) {
    /** @var \Drupal\Core\Entity\Entity\EntityFormDisplay $entityFormDisplay */
    $entityFormDisplay = $this->entityTypeManager->getStorage('entity_form_display')->load($entityFormDisplayId);
    if (empty($entityFormDisplay)) {
      throw new \Exception('Entity form display "' . $entityFormDisplayId . '" could not be loaded');
    }
    $component = $entityFormDisplay->getComponent($fieldId);
    if (empty($component)) {
      throw new \Exception('No component with id: "' . $fieldId . '" found.');
    }
    if ($component['type'] !== self::SOURCE_FIELD_FORMATTER_TYPE) {
      throw new \Exception('Given field "' . $fieldId . '" is not of type text_plain and can not be convertet so.');
    }
    // Set new formatter type:
    $component['type'] = self::TARGET_FIELD_FORMATTER_TYPE;
    // This also removes the dependency on fontawesome_iconpicker in
    // the entity_form_display. Yay!
    $entityFormDisplay->setComponent($fieldId, $component);
    $entityFormDisplay->save();

    return TRUE;
  }

}
