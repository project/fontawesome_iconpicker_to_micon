<?php

namespace Drupal\fontawesome_iconpicker_to_micon\Commands;

use Drush\Commands\DrushCommands;
use Drupal\fontawesome_iconpicker_to_micon\FontawesomeIconpickerToMicoConversionHandler;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class FontawesomeIconpickerToMiconCommands extends DrushCommands {

  /**
   * @var FontawesomeIconpickerToMicoConversionHandler $conversionHandler
   */
  protected $conversionHandler;

  /**
   * Class constructor.
   */
  public function __construct(FontawesomeIconpickerToMicoConversionHandler $conversionHandler) {
    $this->conversionHandler = $conversionHandler;
  }

  /**
   * Converts all fontawesome_iconpicker formatte fields to micon fields.
   *
   * @command fontawesome_iconpicker_to_micon:commandName
   */
  public function convertAll() {
    if ($this->confirm('Do you really want to convert ALL Fontawesome Iconpicker (fontawesome_iconpicker) formatted fields to Micon fields?')) {
      $convertedFields = $this->conversionHandler->convertAll();
      if (!empty($convertedFields)) {
        foreach ($convertedFields as $convertedField) {
          $this->logger()->success(dt('Converted field: @fieldName', ['@fieldName' => $convertedField]));
        }
      }
      else {
        $this->logger()->notice(dt('NO fields to convert.'));
      }

    }
    else {
      $this->logger()->notice(dt('Canceled. Nothing converted.'));
    }
  }

  /**
   * Converts the given fontawesome_iconpicker formatte fields to micon field.
   *
   * @param string $field
   *   The field (storage) machine name to convert.
   *
   * @command fontawesome_iconpicker_to_micon:commandName
   */
  public function convert($field) {
    // @todo: Implement:
    $this->logger->error(dt('Not yet implemented. Use convertAll() or better help improving this module. ;)'));
  }

}
