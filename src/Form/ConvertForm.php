<?php

namespace Drupal\fontawesome_iconpicker_to_micon\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\fontawesome_iconpicker_to_micon\FontawesomeIconpickerToMicoConversionHandler;

/**
 * Provides a Fontawesome iconpicker to micon conversion form.
 */
class ConvertForm extends FormBase {

  /**
   * @var FontawesomeIconpickerToMicoConversionHandler $conversionHandler
   */
  protected $conversionHandler;

  /**
   * Class constructor.
   */
  public function __construct(FontawesomeIconpickerToMicoConversionHandler $conversionHandler) {
    $this->conversionHandler = $conversionHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('fontawesome_iconpicker_to_mico.conversion_handler')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fontawesome_iconpicker_to_micon_convert';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $convertibleFields = $this->conversionHandler->getConvertibleFields();

    if (empty($convertibleFields)) {
      $this->messenger()->addMessage($this->t('No Fontawesome Iconpicker fields found. If you already converted all fields, you may with to uninstall fonatwesome_iconpicker and this module now.'));
      return $form;
    }
    $convertibleFieldsDisplayArray = [];
    foreach ($convertibleFields as $convertibleField) {
      $convertibleFieldsDisplayArray[$convertibleField] = '<strong>' . FontawesomeIconpickerToMicoConversionHandler::extractFieldId($convertibleField) . '</strong> ' . $this->t('in') . ' ' . FontawesomeIconpickerToMicoConversionHandler::extractEntityFormDisplayId($convertibleField) . ' [' . $convertibleField . ']';
    }

    $form['convert_fields'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select fields to convert'),
      '#options' => $convertibleFieldsDisplayArray,
      '#description' => $this->t('Lists all Text (plain) fields which currently use Fontawesome Iconpicker formatter.<br />Choose the fields you wish to convert to Micon Symbol fields.'),
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Convert selected fields'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $convertFields = array_keys($form_state->getValue('convert_fields'));
    try {
      $convertedFields = $this->conversionHandler->convertFields($convertFields);
    } catch (\Exception $e) {
      $this->messenger()->addError($e->getMessage());
    }

    if (!empty($convertedFields)) {
      foreach ($convertedFields as $convertedField) {
        $this->messenger()->addStatus($this->t('Converted field: @fieldName', ['@fieldName' => $convertedField]));
      }
    } else {
      $this->messenger()->addError($this->t('No fields to convert.'));
    }
  }
}
